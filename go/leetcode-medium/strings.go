package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(repeatedStringMatch("abcd", "cdabcdab"))
	fmt.Println(repeatedStringMatch("a", "aa"))
	fmt.Println(repeatedStringMatch("a", "a"))
	fmt.Println(repeatedStringMatch("aa", "a"))
	a := "a"
	b := strings.Repeat("a", 10000)
	fmt.Println(repeatedStringMatch(a, b))
}

// https://leetcode.com/problems/repeated-string-match/
func repeatedStringMatch(a string, b string) int {
	orig := a
	// Naive solution
	ctr := 0
	for ctr <= max(len(a), len(b)) {
		if strings.Contains(a, b) {
			if ctr == 0 {
				return 1
			}
			return ctr
		}
		ctr++
		a = strings.Repeat(orig, ctr)
		if len(a) > 10000 {
			break
		}
	}
	return -1
}

func max (a int, b int) int {
	if a > b {
		return a
	}
	return b
}