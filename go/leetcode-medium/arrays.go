package main

import (
	"fmt"
	"sort"
	"strings"
)

func main() {
	//fmt.Println(groupAnagrams([]string{"tea", "bat", "tab", "foo", "ate", "eat"}))
	//fmt.Println(lengthOfLongestSubstring("abccbadebac"))
	//fmt.Println(minIncrementForUnique([]int{1,2,2}))
	//fmt.Println(minIncrementForUnique2([]int{3,2,1,2,1,7}))
	//fmt.Println(minIncrementForUnique([]int{2,2,2,1}))
	//fmt.Println(numMatchingSubseq("abcde", []string{"a","bb","acd","ace"}))
	//fmt.Println(numMatchingSubseq("abcde", []string{"acd","ace"}))
	fmt.Println(checkSubarraySum([]int{2,2,4,6,7}, 6))
	//fmt.Println(checkSubarraySum([]int{23,2,6,4,7}, 6))

}

// https://leetcode.com/explore/interview/card/top-interview-questions-medium/103/array-and-strings/778/
func groupAnagrams(strs []string) [][]string {
	// General idea:
	// For each string, create a tuple 'key -> string' where key is the string with characters sorted ascending
	// Add each string value to a map using the sorted value as a key

	lt := make(map[string][]string, len(strs))

	for i := range strs {
		// Convert our string to an array of runes
		s := []rune(strs[i])
		// Sort it
		sort.Slice(s, func(j int, k int) bool { return s[j] < s[k] })
		// Using the sorted value as a key, append the original string value to the stored value in the map
		lt[string(s)] = append(lt[string(s)], strs[i])
	}

	// We now have a map with our grouped strings, create a returnable string array
	retVal := make([][]string, len(lt))
	ctr := 0
	for _, v := range lt {
		retVal[ctr] = v
		ctr++
	}
	return retVal
}

// https://leetcode.com/explore/interview/card/top-interview-questions-medium/103/array-and-strings/779/
func lengthOfLongestSubstring(s string) int {
	// Create the lookup table of characters to indices
	seen := make(map[rune]int, len(s))
	// Create our markers
	left, right := 0, 0
	// Maximum substring counter
	maxLen := 0
	// Iterate through the string
	for right < len(s) {
		// If we've seen this char before, then move our left window to the higher of where it was
		// or the previously seen index
		if val, ok := seen[rune(s[right])]; ok {
			left = max(val, left)
		}

		// Check if we have a new max
		maxLen = max(maxLen, right - left + 1)
		// Update the index of the seen map
		seen[rune(s[right])] = right + 1
		right++
	}
	return maxLen
}


// https://leetcode.com/problems/minimum-increment-to-make-array-unique/
func minIncrementForUnique(nums []int) int {
	// Stuff 'em into a map and count
	count := make(map[int]int, len(nums))
	for i := range nums {
		count[nums[i]]++
	}
	// Short-circuit, the map and array are the same length, therefore all unique
	if len(count) == len(nums) {
		return 0
	}
	// Need to find duplicates and determine min increments needed

	// I don't think we need to reshuffle anything, but I think the best strategy
	// would be to start from the highet duplicates and move down (thereby opening new spots)

	// Get the keys and sort them
	var keys []int
	for k := range count {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	// Starting from the highest key in the map, increment out
	ctr := 0
	for i := len(keys) - 1; i >= 0; i-- {
		curKey := keys[i]
		// If we don't need to increment, keep going!
		if count[curKey] <= 1 {
			continue
		}
		// Continue incrementing the current key until there's an empty spot
		newKey := curKey
		// While the current key is > 0 (handles count > 2)
		for count[curKey] > 1 {
			// While the new key is > 0 (non-empty)
			for count[newKey] > 0 {
				ctr++
				newKey++
			}
			// We've found a spot, decrement 'i' and increment the new spot
			count[curKey]--
			count[newKey]++
			// If we still have duplicates in the current key, reset newKey
			if count[curKey] > 1 {
				newKey = curKey
			}
		}
	}
	return ctr
}

// This looks like a special case of similar 'minimum steps' problems
func minIncrementForUnique2(nums []int) int {
	// Null case
	if len(nums) == 0 {
		return 0
	}
	// Sort the array
	sort.Ints(nums)
	// Initialize counters
	steps := 0
	// Next potentially free value
	nextVal := nums[0] + 1
	for i := 1; i < len(nums); i++ {
		// If our current value is lte the previous one
		// (either an original duplicate or we've incremented past it)
		if nums[i] <= nums[i - 1] {
			// Increment steps as the difference between the next free slot and the current value
			steps += nextVal - nums[i]
			// Adjust current value to next value
			nums[i] = nextVal
			// Increment next value to another potentially free spot
			nextVal++
		} else {
			// Adjust the next value to be the greater of the current value +1, or the original next value + 1
			nextVal=max(nums[i]+1, nextVal+1)
		}
	}
	return steps
}

// https://leetcode.com/problems/number-of-matching-subsequences/
func numMatchingSubseq(s string, words []string) int {
	// Return counter
	ctr := 0

	// For each word in words
	for i := range words {
		word := words[i]
		// Initialize our previous index marker
		prevIdx := -1
		// For each character in the word
		for j := range word {
			// Check the input string (continually left trimmed) if we have the char in the remainder
			subStr := s[prevIdx+1:]
			idx := strings.Index(subStr, string(word[j]))
			// If the char isn't present, it's not a subsequence
			if idx == -1 {
				break
			}
			// Add this offset to the previous offset, and move to the next one
			prevIdx += idx + 1

			// If we're at the end of the word and made it this far, we must be a subsequence
			if j == len(word) - 1 {
				ctr++
			}
		}
	}
	return ctr
}

// https://leetcode.com/problems/continuous-subarray-sum/

/*
https://leetcode.com/problems/continuous-subarray-sum/discuss/99499/Java-O(n)-time-O(k)-space/103585

For e.g. in case of the array [23,2,6,4,7] the running sum is [23,25,31,35,42]
and the remainders are [5,1,1,5,0]. We got remainder 5 at index 0 and at index 3.
That means, in between these two indexes we must have added a number which is multiple of the k.
Hope this clarifies your doubt :)
 */
func checkSubarraySum(nums []int, k int) bool {
	sum := 0
	seen := make(map[int]int, k + 1)
	seen[0] = -1

	for i := range nums {
		sum += nums[i]
		mod := sum % k
		if v, ok := seen[mod]; ok {
			if i - v > 1 {
				return true
			}
		} else {
			seen[mod] = i
		}
	}
	return false
}

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}


