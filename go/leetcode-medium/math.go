package main

import "fmt"

func main() {
	fmt.Println(clumsy(1))
	fmt.Println(clumsy(5))
	fmt.Println(clumsy(10))
}

// https://leetcode.com/problems/clumsy-factorial/
func clumsy(n int) int {
	// MDAS in order
	op := 0
	res := n
	stack := n
	for i := n-1; i > 0; i-- {
		if op == 0 {
			stack *= i
			op++
		} else if op == 1 {
			stack /= i
			if i == n - 2 {
				res += stack
			} else {
				res -= stack
			}
			stack = 0
			op++
		} else if op == 2 {
			res += i
			op++
		} else if op == 3 {
			// No-op, the subtraction is handled in op == 1 from the stack
			op = 0
		}
	}
	return res
}
