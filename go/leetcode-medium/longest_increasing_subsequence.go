package main

import (
	"fmt"
	"sort"
)

func main() {
	fmt.Println(lengthOfLIS([]int{3,4,1,2,3,2,3,4,5}))
}

// https://leetcode.com/problems/longest-increasing-subsequence/

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}


func lengthOfLIS(nums []int) int {
	// Sorted array of subsequence seen so far
	var dp []int

	for _, num := range nums {
		// Find the location in dp where this number would insert into
		i := sort.SearchInts(dp, num)
		// If it's the end, append to it
		if i == len(dp) {
			dp = append(dp, num)
		} else {
			// Otherwise, insert this new value back into dp to potentially use
			// in a new sequence
			dp[i] = num
		}
	}

	// The longest subsequence will have been found in 'dp' at some point, whose
	// length will be represented by the number of elems in the current array
	return len(dp)
}