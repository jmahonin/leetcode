package main

import (
	"fmt"
	"strings"
)

func main() {
	// Section 2
	//fizzBuzz()
	//evenEnded()
	//calcMax()
	//wordCount()

	// Section 3
}

// Challenge 2.1
func fizzBuzz() {
	for i:= 1; i < 20; i++ {
		if i % 15 == 0 {
			fmt.Printf("%v: FizzBuzz\n", i)
		} else if i % 5 == 0 {
			fmt.Printf("%v: Buzz\n", i)
		} else if i % 3 == 0 {
			fmt.Printf("%v: Fizz\n", i)
		}
	}
}

// Challenge 2.2
func evenEnded() {
	ctr := 0
	for i := 1000; i <= 9999; i++ {
		for j := i; j < 9999; j++ {
			str := fmt.Sprintf("%d", i * j)
			if str[0] == str[len(str) - 1] {
				ctr++
			}
		}
	}
	fmt.Println(ctr)
}
// Challenge 2.3
func findMax() {
	// Calculate maximal value in a slice

	nums := []int{16, 8, 42, 4, 23, 15}
	var max int

	for _, val := range nums {
		if val > max {
			max = val
		}
	}
	fmt.Println(max)
}

// Challenge 2.4
func wordCount() {
	text := `
		Needles and pins
		Needles and pins
		Sew me a sail
		To catch me the wind
		`

	count := map[string]int{}

	// Lowercase and split the text block above
	// Is this idiomatic Go? Not sure.
	for _, v := range strings.Fields(strings.ToLower(text)) {
		// Count each word occurrence
		count[v] = count[v] + 1
	}

	// Idiomatic solution given
	/*
	for _, v := range strings.Fields(text) {
		count[strings.ToLower(v)]++
	}
	 */

	fmt.Println(count)
}
