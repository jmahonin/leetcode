package main

import "fmt"

func main() {
	//fmt.Println(countPrimes(5))
	//fmt.Println(isPowerOfThree(27))
	//fmt.Println(isPowerOfThreeFast(30))
	fmt.Println(romanToIntMap("LVIII"))
}

func countPrimes(n int) int {
	if n <= 1 {
		return 0
	}
	sieve := make([]int, n)

	// Initialize the sieve. 1s are non-prime
	for i := 2; i < n/2+1; i++ {
		// No need to sieve multiples
		if sieve[i] == 1 {
			continue
		}
		for j := 2; i*j < n; j++ {
			sieve[i*j] = 1
		}
	}

	// Count zeros, skip 0 and 1
	ctr := 0
	for i := 2; i < n; i++ {
		if sieve[i] == 0 {
			ctr++
		}
	}

	return ctr
}

// n = 9, true (3 ^ 2) = 3 * 3
// n = 27, true (3 ^ 3) = 3 * 3 * 3

func isPowerOfThree(n int) bool {
	// Is there an integer x such that n == 3^x

	// Naive solution would be to iterate and count. This seems slow and expensive
	// Better solution would be to iteratively divide by 3
	// There's probably some cool bit-shifting or base-changing method to make this fast

	// If we're not even a multiple of 3, abort
	if n == 1 {
		return true
	}
	for n != 3 {
		if n%3 != 0 || n == 0 {
			return false
		}
		n = n / 3
	}
	return true
}

func isPowerOfThreeFast(n int) bool {
	// 3^19 is the upper bound that fits into int32
	// Let's pre-optimize!
	switch n {
	case 1, 3, 9, 27, 81, 243, 729, 2187, 6561, 19683, 59049, 177147, 531441,
		1594323, 4782969, 14348907, 43046721, 129140163, 387420489, 1162261467:
		return true
	}
	return false
}


/*
I             1
V             5
X             10
L             50
C             100
D             500
M             1000

Addendum:
0-9: I, II, III, IV, V, VI, VII, VIII, IX, X

XL: 40
XC: 90
CD: 400
CM: 900

 */

func romanToInt(s string) int {
	// Tokenize each symbol (could be up to 3!)
	// Add to current total
	curTotal := 0
	for curPos := 0; curPos < len(s); curPos++ {
		fmt.Printf("Current total: %d\n", curTotal)
		// Two-symbol
		if curPos+1 < len(s) {
			fmt.Printf("2 symbol: %s\n", s[curPos:curPos+2])
			switch s[curPos : curPos+2] {
			case "IV":
				curTotal += 4;
				curPos++;
				continue
			case "IX":
				curTotal += 0;
				curPos++;
				continue
			case "XL":
				curTotal += 40;
				curPos++;
				continue
			case "XC":
				curTotal += 90;
				curPos++;
				continue
			case "CD":
				curTotal += 400;
				curPos++;
				continue
			case "CM":
				curTotal += 900;
				curPos++;
				continue
			}
		}
		// 1 Symbol
		fmt.Printf("1 symbol: %s\n", string(s[curPos]))
		switch s[curPos] {

		// One-symbol characters
		case 'I':
			curTotal += 1;
			continue
		case 'V':
			curTotal += 5;
			continue
		case 'X':
			curTotal += 10;
			continue
		case 'L':
			curTotal += 50;
			continue
		case 'C':
			curTotal += 100;
			continue
		case 'D':
			curTotal += 500;
			continue
		case 'M':
			curTotal += 1000;
			continue
		}
	}
	return curTotal
}

func romanToIntMap(s string) int {
	lt := map[rune]int{
		'I': 1,
		'V': 5,
		'X': 10,
		'L': 50,
		'C': 100,
		'D': 500,
		'M': 1000,
	}

	// Initialize sum with the value of the last character
	sum := lt[rune(s[len(s)-1])]

	// Work our way backwards. If the value we read is lower than the one after it
	for i := len(s)-2; i >= 0; i-- {
		if lt[rune(s[i])] < lt[rune(s[i+1])] {
			sum -= lt[rune(s[i])]
		} else {
			sum += lt[rune(s[i])]
		}
	}
	return sum
}