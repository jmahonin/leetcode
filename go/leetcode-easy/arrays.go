package main

import "fmt"

func main() {
	//rotate([]int{0,1,2,3,4,5,6}, 2)
	//rotate([]int{-1}, 2)
	//fmt.Println(singleNumber([]int{0,0,1,3,1,2,2}))
	//fmt.Println(intersect([]int{2,2,1,1,0,0}, []int{0,1,2}))
	//fmt.Println(twoSum([]int{2,7,11,15}, 9))
	//fmt.Println(maxProfit([]int{7,1,5,3,6,4}))
	//fmt.Println(plusOne([]int{1, 9, 9, 9}))
	//nums := []int{1, 0, 2, 0, 3, 4}
	//moveZeroes(nums)

	/*
		board := [][]byte{
			{'5','3','.','.','7','.','.','.','.'},
			{'6','.','.','1','9','5','.','.','.'},
			{'.','9','8','.','.','.','.','6','.'},
			{'8','.','.','.','6','.','.','.','3'},
			{'4','.','.','8','.','3','.','.','1'},
			{'7','.','.','.','2','.','.','.','6'},
			{'.','6','.','.','.','.','2','8','.'},
			{'.','.','.','4','1','9','.','.','5'},
			{'.','.','.','.','8','.','.','7','9'},
		}

		fmt.Println(isValidSudoku(board))
	*/

	/*
	matrix := [][]int{
		{1,2,3},
		{4,5,6},
		{7,8,9},
	}
	*/

	matrix := [][]int{{5,1,9,11},{2,4,8,10},{13,3,6,7},{15,14,12,16}}


	//rotateMatrix(matrix)
	rotateMatrix2(matrix)
	fmt.Println(matrix)

}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/646/

func rotate(nums []int, k int) {
	// If k is larger than length, modulo it down
	k = k % len(nums)
	fmt.Println(nums)
	// Rotate the whole array
	rotateInPlace(nums, 0, len(nums)-1)
	// Rotate first k
	rotateInPlace(nums, 0, k-1)
	// Rotate last k
	rotateInPlace(nums, k, len(nums)-1)
	fmt.Println(nums)
}

func rotateNewArray(nums []int, k int) {
	// Simple solution, NOT in place
	// Create a new array of size 'k', copy the end 'k' values into it, drop them, prepend
	rotation := nums[len(nums)-k : len(nums)]
	rotation = append(rotation, nums[0:len(nums)-k]...)
	fmt.Println(rotation)
}

func rotateInPlace(nums []int, start int, end int) {
	for start < end {
		temp := nums[start]
		nums[start] = nums[end]
		nums[end] = temp
		start += 1
		end -= 1
	}
}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/549/
// You must implement a solution with a linear runtime complexity and use only constant extra space.

func singleNumber(nums []int) int {
	// XOR each element, pairs will cancel out
	ctr := nums[0]
	for i := 1; i < len(nums); i++ {
		ctr = ctr ^ nums[i]
	}
	return ctr
}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/674/
func intersect(nums1 []int, nums2 []int) []int {
	// Counters
	ctr1 := make(map[int]int)
	ctr2 := make(map[int]int)

	// Count 'em up
	for i := range nums1 {
		ctr1[nums1[i]] += 1
	}

	for i := range nums2 {
		if ctr1[nums2[i]] > 0 {
			// Increment the intersection counter
			ctr2[nums2[i]] += 1
			// Decrement the initial counter (so we end up with the minimum of each array)
			ctr1[nums2[i]] -= 1
		}
	}

	// So now ctr2 has the intersection w/ counts, turn it into an array
	var ret []int
	for k, v := range ctr2 {
		// Make a new slice for each occurence of length v
		new := make([]int, v)
		// Fill it with k
		for i := range new {
			new[i] = k
		}
		// Append this new array to the return value
		ret = append(ret, new...)
	}
	return ret
}

func min(a int, b int) int {
	if a < b {
		return a
	}
	return b
}

func twoSum(nums []int, target int) []int {
	offset := make(map[int]int)
	for i := range nums {
		v := nums[i]
		if val := offset[v]; val != 0 {
			return []int{i, val - 1}
		}
		// Record the offset index in the map (e.g. 9 - 7 = 2, then 2 -> 0)
		offset[target-v] = i + 1
	}
	return []int{}
}

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/564/
/*
Input: prices = [7,1,5,3,6,4]
Output: 7
Explanation: Buy on day 2 (price = 1) and sell on day 3 (price = 5), profit = 5-1 = 4.
Then buy on day 4 (price = 3) and sell on day 5 (price = 6), profit = 6-3 = 3.
*/

// If we draw it out, we can see that we can optimize profit by selling any time
// the price is higher than it was before (implied buying). Thus, we don't need
// to do complicated backtracking, we can just find any time that there is a price
// differential on subsequent days where price(t+1) > price(t) and sum up profits

func maxProfit(prices []int) int {
	if len(prices) <= 1 {
		return 0
	}
	maxProf := 0
	for i := 1; i < len(prices); i++ {
		prof := prices[i] - prices[i-1]
		if prof > 0 {
			maxProf += prof
		}
	}
	return maxProf
}

// [4,3,2,1] -> [4, 3, 2, 2]

func plusOne(digits []int) []int {
	// Take care of the trivial case
	if digits[len(digits)-1] < 9 {
		digits[len(digits)-1] += 1
		return digits
	}

	carry := true
	for i := len(digits) - 1; i >= 0; i-- {
		// If we have a carry and the digit is 9, bump to 0 and continue
		if carry == true && digits[i] == 9 {
			digits[i] = 0
			carry = true
		} else {
			// Otherwise, just increment the value and break
			digits[i] += 1
			carry = false
			break
		}
	}

	// Handle overflow
	if carry == true {
		digits = append([]int{1}, digits...)
	}

	return digits
}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/567/
// [0,1,0,3,12] --> [1, 3, 12, 0, 0]
// Maintain relative order
func moveZeroes(nums []int) {
	// Let's count the zeroes we see
	// Maintain a pointer to the current writable slot
	// And another pointer to the current reading slot

	ctr := 0
	i := 0
	for j := 0; j < len(nums); j++ {
		// i is the current write slot, j is the current read slot
		// If we found a zero, increment the counter
		if nums[j] == 0 {
			ctr += 1
		} else {
			// We found a number, move it
			nums[i] = nums[j]
			i++
		}
	}
	// I think we've moved them all back, and we have a counter of zeros
	// Fill zeroes from i
	for j := i; j < len(nums); j++ {
		nums[j] = 0
	}
}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/769/
func isValidSudoku(board [][]byte) bool {
	// Check an array for duplicates
	checkDuplicates := func(arr []byte) bool {
		// Use the numerical value as an index, so allocate an extra byte
		check := make([]byte, len(arr)+1)
		for i := range arr {
			if arr[i] == '.' {
				continue
			}
			// Convert byte offset to array offset ('0' -> 0)
			val := arr[i] - 48
			if check[val] > 0 {
				return false
			}
			check[val] += 1
		}
		return true
	}

	// Boxes are [0, 1, 2]
	//           [3, 4, 5]
	//           [6, 7, 9]

	// e.g.: box 0 is [0][0], [0][1], [0][2]
	//				  [1][0], [1][1], [1][2]
	//                [2][0], [2][1], [2][2]

	for i := 0; i < 9; i++ {
		// Check rows
		if !checkDuplicates(board[i][0:9]) {
			return false
		}

		// Check lines. i becomes our column
		var line []byte
		for j := 0; j < 9; j++ {
			line = append(line, board[j][i])
		}
		if !checkDuplicates(line) {
			return false
		}
	}

	// Check boxes
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			rowStart := 3 * i
			colStart := 3 * j
			var box []byte
			box = append(box, board[rowStart][colStart:colStart+3]...)
			box = append(box, board[rowStart+1][colStart:colStart+3]...)
			box = append(box, board[rowStart+2][colStart:colStart+3]...)
			if !checkDuplicates(box) {
				return false
			}
		}
	}

	return true
}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/770/

// [[5,1,9,11],[2,4,8,10],[13,3,6,7],[15,14,12,16]]
// 5 @ 0,0 ==> 0,4
// 1 @ 0,1 ==> 1,4
// 3 @ 2,1 ==> 2,2

func rotateMatrix(matrix [][]int) {
	// Generally, if we transpose each row into a column, we can arrange those columns into a rotation
	// This would require extra space

	// Instead, work down from the outside layers, rotating each spot in place
	n := len(matrix) - 1
	// For each layer
	for l := 0; l < len(matrix) / 2; l++ {
		// For each element
		for i := 0; i < len(matrix) - 2*l - 1; i++ {
			// Swap top-left -> top-right
			matrix[l][l+i], matrix[l+i][n-l] = matrix[l+i][n-l], matrix[l][l+i]

			// Swap top-left -> bottom-right
			matrix[l][i+l], matrix[n-l][n-l-i] = matrix[n-l][n-l-i], matrix[l][i+l]

			// Swap top-left -> bottom-left
			matrix[l][i+l], matrix[n-l-i][l] = matrix[n-l-i][l], matrix[l][i+l]
		}
	}
}

func rotateMatrix2(matrix [][]int) {
	transpose(matrix)
	reverse(matrix)
}
func transpose(matrix [][]int) {
	for n := 0; n < len(matrix) - 1; n++ {
		for m := n + 1; m < len(matrix); m++ {
			matrix[n][m], matrix[m][n] = matrix[m][n], matrix[n][m]
		}
	}
}

func reverse(matrix [][]int) {
	// For each row
	for i := 0; i < len(matrix); i++ {
		for j, k := 0, len(matrix)-1; j < k; j, k = j+1, k-1 {
			matrix[i][j], matrix[i][k] = matrix[i][k], matrix[i][j]
		}
	}
}
