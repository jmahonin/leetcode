// https://leetcode.com/problems/exclusive-time-of-functions/
package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	//logs := []string{"0:start:0","1:start:2","1:end:5","0:end:6"}
	logs := []string{"0:start:0","0:start:2","0:end:5","0:start:6","0:end:6","0:end:7"}
	ret := exclusiveTime(1, logs)
	fmt.Println(ret)
}

type Func struct {
	ID int
	Start int
	Duration int
}
func exclusiveTime(n int, logs []string) []int {
	var stack []Func
	times := make([]int, n)

	var prevTime = 0

	for i := range logs {
		parts := strings.Split(logs[i], ":")
		id, _ := strconv.Atoi(parts[0])
		status := parts[1]
		ts, _ := strconv.Atoi(parts[2])

		if status == "start" {
			f := Func{id, ts, 0}
			if len(stack) > 0 {
				stack[len(stack)-1].Duration = ts - prevTime
			}
			stack = append(stack, f)
		} else {
			top := stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			times[top.ID] += top.Duration + ts - prevTime + 1
		}

		prevTime = ts

	}
	return times

}