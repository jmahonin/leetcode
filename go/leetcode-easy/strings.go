package main

import (
	"fmt"
	"math"
	"strings"
)

func main() {
	//reverseString([]byte("This is a test"))
	//fmt.Println(firstUniqChar("aabb"))
	//fmt.Println(myAtoi("20000000000000000000"))
	//fmt.Println(strStr("a", "a"))
	fmt.Println(isPalindrome("A man, a plan, a canal: Panama"))
	fmt.Println(isPalindrome("00"))
}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/127/strings/879/

func reverseString(s []byte)  {
	start := 0
	end := len(s) - 1
	for start < end {
		temp := s[start]
		s[start] = s[end]
		s[end] = temp
		start += 1
		end -= 1
	}
	//fmt.Println(string(s))
}

func firstUniqChar(s string) int {
	charMap := make(map[rune]int)
	// Count the character frequencies
	for _, c := range s {
		charMap[c] += 1
	}
	for i, c := range s {
		if charMap[c] == 1 {
			return i
		}
	}
	return -1
}

func myAtoi(s string) int {
	doneWhitespace := false
	doneSign := false
	doneLeadingZeroes := false
	isNegative := false

	var numericVals []rune

	for _, c := range s {
		// Handle leading whitespace
		if c == ' ' && !doneWhitespace {
			continue
		}
		doneWhitespace = true

		// Handle leading sign
		if c == '-' && !doneSign {
			isNegative = true
			doneSign = true
			continue
		} else if c == '+' && !doneSign {
			doneSign = true
			continue
		}

		if c == '0' && !doneLeadingZeroes {
			continue
		}
		doneLeadingZeroes = true
		doneSign = true

		// Process numeric characters
		if c >= '0' && c <= '9' {
			doneSign = true
			numericVals = append(numericVals, c)
			continue
		}

		// Invalid character, break
		break
	}
	fmt.Printf("%v, %c %v\n", isNegative, numericVals, numericVals)

	MIN_VAL := -1 * int(math.Pow(2.0, 31))
	MAX_VAL := int(math.Pow(2.0, 31)) - 1

	retVal := 0
	mag := len(numericVals)
	if mag > 10 && !isNegative {
		return MAX_VAL
	} else if mag > 10 && isNegative {
		return MIN_VAL
	}

	for i, v := range numericVals {
		// e.g.: v == '5' == int(53). Multiply by factor of 10 based on index
		// This probably needs better handling here
		retVal += int(v - 48) * int(math.Pow10(mag - i - 1))
	}
	if isNegative {
		retVal = retVal * -1
	}

	if retVal < MIN_VAL {
		return MIN_VAL
	} else if retVal > MAX_VAL {
		return MAX_VAL
	}

	return retVal
}

func strStr(haystack string, needle string) int {
	if len(needle) == 0 {
		return 0
	}
	if len(needle) > len(haystack) {
		return -1
	}
	for i := range haystack {
		j, k := i, 0
		for j <= len(haystack) - 1 && haystack[j] == needle[k] {
			if k == len(needle) - 1 {
				return i
			}
			j++
			k++
		}
	}
	return -1
}

func isPalindrome(s string) bool {
	// Lowercase the input string, filter anything but a-z
	s = strings.ToLower(s)
	var tmp []rune
	for _, v := range s {
		if (v >= '0' && v <= '9') || (v >= 'a' && v <= 'z') {
			tmp = append(tmp, v)
		}
	}


	s = strings.ReplaceAll(string(tmp), " ", "")
	for i, j := 0, len(s) - 1; i < j; i, j = i+1, j-1 {
		if s[i] != s[j] {
			return false
		}
	}
	return true
}