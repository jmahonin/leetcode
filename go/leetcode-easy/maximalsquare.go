// https://leetcode.com/problems/maximal-rectangle/
package main

import "fmt"

func main() {
	//matrix := [][]byte{{0, 1}}
	matrix := [][]uint8{[]uint8{0x31, 0x30, 0x31, 0x30, 0x30}, []uint8{0x31, 0x30, 0x31, 0x31, 0x31}, []uint8{0x31, 0x31, 0x31, 0x31, 0x31}, []uint8{0x31, 0x30, 0x30, 0x31, 0x30}}
	//matrix := [][]byte{{0x31}}
	printSquare(matrix)
	fmt.Println(maximalSquare(matrix))
}

func printSquare(matrix [][]byte) {
	for i := range matrix {
		fmt.Printf("%d\n", matrix[i])
	}
}

func printSquareInt(matrix [][]int) {
	for i := range matrix {
		fmt.Printf("%d\n", matrix[i])
	}
}

func maximalSquare(matrix [][]byte) int {
	m := len(matrix)
	n := len(matrix[0])

	if m == n && m == 1 {
		if matrix[0][0] == 0x31 {
			return 1
		}
	}
	// Create the memo array
	dp := make([][]int, m+1)
	for i := range dp {
		dp[i] = make([]int, n+1)
	}

	// Iterate through each value, calculating areas as we go
	maxLen := 0
	for i := 1; i <= m; i++ {
		for j:= 1; j <= n; j++ {
			if matrix[i-1][j-1] == 1 || matrix[i-1][j-1] == 0x31 {
				// Take the min of the surrounding, plus 1
				dp[i][j] = min(min(dp[i-1][j], dp[i][j-1]), dp[i-1][j-1]) + 1
				maxLen = max(maxLen, dp[i][j])
			}
			fmt.Println()
			printSquareInt(dp)
		}
	}


	return maxLen * maxLen
}

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}
func min(a int, b int) int {
	if a < b {
		return a
	}
	return b
}
