// From LinkedIn Learning Go Course
package main

import (
	"bufio"
	"crypto"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
)

func main() {
	// Section 2
	//fizzBuzz()
	//evenEnded()
	//calcMax()
	//wordCount()

	// Section 3
	//testGetURL("https://google.ca")

	// Section 4.1
	//testSquare()
	// Section 4.2
	//testCapper()

	// Section 5
	//testKillServer()
	//testChannels()

	// Section 6
	//testConcurrentMD5()

	// Section 8
	//testUserInfo()
	kvServer()
}

// Challenge 2.1
func fizzBuzz() {
	for i := 1; i < 20; i++ {
		if i%15 == 0 {
			fmt.Printf("%v: FizzBuzz\n", i)
		} else if i%5 == 0 {
			fmt.Printf("%v: Buzz\n", i)
		} else if i%3 == 0 {
			fmt.Printf("%v: Fizz\n", i)
		}
	}
}

// Challenge 2.2
func evenEnded() {
	ctr := 0
	for i := 1000; i <= 9999; i++ {
		for j := i; j < 9999; j++ {
			str := fmt.Sprintf("%d", i*j)
			if str[0] == str[len(str)-1] {
				ctr++
			}
		}
	}
	fmt.Println(ctr)
}

// Challenge 2.3
func findMax() {
	// Calculate maximal value in a slice

	nums := []int{16, 8, 42, 4, 23, 15}
	var max int

	for _, val := range nums {
		if val > max {
			max = val
		}
	}
	fmt.Println(max)
}

// Challenge 2.4
func wordCount() {
	text := `
		Needles and pins
		Needles and pins
		Sew me a sail
		To catch me the wind
		`

	count := map[string]int{}

	// Lowercase and split the text block above
	// Is this idiomatic Go? Not sure.
	for _, v := range strings.Fields(strings.ToLower(text)) {
		// Count each word occurrence
		count[v] = count[v] + 1
	}

	// Idiomatic solution given
	/*
		for _, v := range strings.Fields(text) {
			count[strings.ToLower(v)]++
		}
	*/

	fmt.Println(count)
}

// Challenge 3.1
func testGetURL(url string) {
	resp, err := getURL(url)
	if err != nil {
		fmt.Printf("ERROR: %s\n", err)
	} else {
		fmt.Println(resp)
	}
}

func getURL(url string) (string, error) {
	response, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	ctype := response.Header.Get("Content-Type")
	if ctype == "" {
		return "", fmt.Errorf("Can't find key: Content-Type")
	}
	return ctype, nil
}

// Challenge 4.1

type Point struct {
	X int
	Y int
}

func (p *Point) Move(dx int, dy int) {
	p.X += dx
	p.Y += dy
}

type Square struct {
	Center Point
	Length int
}

func (s *Square) Move(dx int, dy int) {
	s.Center.Move(dx, dy)
}

func (s *Square) Area() int {
	return s.Length * s.Length
}

func NewSquare(x int, y int, length int) (*Square, error) {
	if length < 0 {
		return nil, fmt.Errorf("length must be > 0")
	}
	s := &Square{
		Point{x, y},
		length,
	}

	return s, nil
}

func testSquare() {
	sq, _ := NewSquare(0, 0, 5)
	fmt.Printf("%+v\n", sq)
	fmt.Printf("Area: %v\n", sq.Area())
	sq.Move(5, 5)
	fmt.Printf("%+v\n", sq)
	fmt.Printf("Area: %v\n", sq.Area())

	sq, err := NewSquare(0, 0, -1)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

type Capper struct {
	wtr io.Writer
}

func (c *Capper) Write(p []byte) (n int, err error) {
	if len(p) == 0 {
		return 0, fmt.Errorf("input must not be empty")
	}
	out := []byte(strings.ToUpper(string(p)))

	// Actual assignment code, bleh
	/*
		// Calculate diff, I think it's -26 right?
		diff := byte('a' - 'A')
		out := make([]byte, len(p))
		for i, c := range p {
			if c >= 'a' && c <= 'z' {
				c -= diff
			}
			out[i] = c
		}
	*/

	return c.wtr.Write(out)
}

func testCapper() {
	c := &Capper{os.Stdout}
	fmt.Fprintf(c, "Hello World!")
}

func killServer(pidFile string) error {
	// Open pid file
	data, err := ioutil.ReadFile(pidFile)
	if err != nil {
		return errors.Wrap(err, "Can't open file")
	}

	// Try convert to an int
	pid, err := strconv.Atoi(string(data))
	if err != nil {
		return errors.Wrap(err, "Couldn't parse PID")
	}

	// Print it
	fmt.Printf("Got PID %v\n", pid)
	return nil
}

func testKillServer() {
	err := killServer("/tmp/test.pid")
	if err != nil {
		log.Fatal(err)
	}
}

func testChannels() {
	// Define our urls
	urls := []string{
		"https://golang.org",
		"https://api.github.com",
		"https://httpbin.org/xml",
	}

	// Make a channel
	ch := make(chan string)

	// Call the function for each url
	for _, url := range urls {
		go func(url string, out chan string) {
			resp, err := http.Get(url)
			if err != nil {
				out <- fmt.Sprintf("Error: %s\n", err)
				return
			}
			defer resp.Body.Close()

			ctype := resp.Header.Get("Content-Type")
			out <- fmt.Sprintf("%s -> %s", url, ctype)
		}(url, ch)
	}

	// This doesn't work because it'll block when the queue is empty
	/*
		for elem := range ch {
			fmt.Printf("Got %s\n", elem)
		}
	*/

	// Alternatively, we'll iterate once per input item
	for range urls {
		// Read from the channel and print it
		out := <-ch
		fmt.Println(out)
	}
}

func testConcurrentMD5() {
	// Input locations
	const folderPrefix = "/Users/jmahonin/Downloads/Ex_Files_Go_EssT"
	checksum_file := folderPrefix + "/md5sum.txt"
	files := []string{
		"nasa-00.log",
		"nasa-01.log",
		"nasa-02.log",
		"nasa-03.log",
		"nasa-04.log",
		"nasa-05.log",
		"nasa-06.log",
		"nasa-07.log",
		"nasa-08.log",
		"nasa-09.log",
	}

	// Load checksum file
	file, err := os.Open(checksum_file)
	if err != nil {
		log.Fatal("Could not open checksum file: %s", err)
	}
	defer file.Close()

	// Read each line into a map
	checksums := map[string]string{}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		s := strings.Fields(scanner.Text())
		checksums[s[1]] = s[0]
	}

	if err := scanner.Err(); err != nil {
		log.Fatal("Error parsing checksum file: %s", err)
	}

	// We're going to process concurrently with a wait group to sync
	var wg sync.WaitGroup

	// Need to open each file for reading to calculate Md5 sums
	for _, file := range files {
		wg.Add(1)
		go func(file string) {
			defer wg.Done()

			// Try read the file
			data, err := ioutil.ReadFile(folderPrefix + "/" + file)
			if err != nil {
				log.Fatal("Error loading file: %s", err)
			}

			// Create the hash and use it
			md5 := crypto.MD5.New()
			md5.Write(data)
			bs := fmt.Sprintf("%x", md5.Sum(nil))

			if checksums[file] != bs {
				fmt.Printf("Bad checksum! File: %s, Calculated: %s, Stored: %s\n", file, bs, checksums[file])
			} else {
				// Check if this lines up with the stored checksums
				fmt.Printf("File '%s' matched checksum '%s'\n", file, bs)
			}
		}(file)
	}

	wg.Wait()
}

type User struct {
	Name 		string	`json:"login"`
	PublicRepos int 	`json:"public_repos"`
}

func userInfo(login string) (*User, error) {
	const GH_USERS_API = "https://api.github.com/users/"

	if len(login) == 0 {
		return nil, fmt.Errorf("Invalid user given")
	}

	resp, err := http.Get(GH_USERS_API + login)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// Got a response, marshal it into the User struct
	res := &User{}
	if err := json.NewDecoder(resp.Body).Decode(res); err != nil {
		return nil, err
	}

	return res, nil
}

func testUserInfo() {
	res, err := userInfo("jmahonin")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%+v\n", res)
}


// Our "database"
//db map[string]interface {}

type DBStruct struct {
	mu sync.Mutex
	db map[string]interface{}
}

type Entry struct {
	Key string `json:"key"`
	Value interface{} `json:"value"`
}

var DB = &DBStruct{db: make(map[string]interface{})}

func reqHandler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	req := &Entry{}
	if err := json.NewDecoder(r.Body).Decode(req); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	log.Printf("%+v", req)
	if r.Method == http.MethodPost {
		// Synchronize write access
		DB.mu.Lock()
		defer DB.mu.Unlock()
		DB.db[req.Key] = req.Value
		fmt.Fprintf(w, "OK\n")
		return
	} else if r.Method == http.MethodGet {
		// Read access, let 'er rip with no locks?
		resp := &Entry{req.Key, DB.db[req.Key]}
		w.Header().Set("Content-Type", "application/json")
		if err := json.NewEncoder(w).Encode(resp); err != nil {
			log.Printf("can't encode %v - %v", resp.Key, resp.Value)
		}
	} else {
		http.Error(w, "Unsupported method" , http.StatusBadRequest)
		return
	}

}

func kvServer() {
	http.HandleFunc("/db", reqHandler)
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}