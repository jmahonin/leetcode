package main

import (
	"fmt"
	"math/rand"
)

func main() {
	/*
	 * obj := Constructor(nums);
	 * param_1 := obj.Reset();
	 * param_2 := obj.Shuffle();
	 */

	obj := Constructor([]int{100,2,3,4,5,6,7,8,-900})
	fmt.Printf("%+v\n", obj)
	fmt.Printf("%d\n", obj.Shuffle())
	obj.Reset()
	fmt.Printf("%+v\n", obj)
	fmt.Printf("%d\n", obj.Shuffle())
	obj.Reset()
	fmt.Printf("%+v\n", obj)
	fmt.Printf("%+v\n", obj)
	fmt.Printf("%d\n", obj.Shuffle())
	obj.Reset()
	fmt.Printf("%+v\n", obj)
	fmt.Printf("%d\n", obj.Shuffle())
	obj.Reset()
}

type Solution struct {
	nums []int
}


func Constructor(nums []int) Solution {
	return Solution{nums}
}


/** Resets the array to its original configuration and return it. */
func (this *Solution) Reset() []int {
	return this.nums
}


/** Returns a random shuffling of the array. */
func (this *Solution) Shuffle() []int {
	ret := make([]int, len(this.nums))
	copy(ret, this.nums)

	// Given a random permutation of length n
	// (4, 1, 2, 3), index i goes to new location v
	/*
	for i, v := range rand.Perm(len(this.nums)) {
		ret[i], ret[v] = ret[v], ret[i]
	}
	*/

	rand.Shuffle(len(ret), func(i, j int) {
		ret[i], ret[j] = ret[j], ret[i]
	})
	return ret
}