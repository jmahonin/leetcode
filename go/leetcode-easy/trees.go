package main

import "fmt"

func main() {
	tree := &TreeNode{
		3,
		&TreeNode{9, nil, &TreeNode{4, nil, nil}},
		&TreeNode{9, &TreeNode{4, nil, nil}, nil},
		}

	//bst := &TreeNode{2, &TreeNode{1, nil, nil}, &TreeNode{3, nil, nil}}
	/*
	bst := &TreeNode{5,
		&TreeNode{1, nil, nil},
		&TreeNode{6,
			&TreeNode{3, nil, nil},
			&TreeNode{7, nil, nil}}}
	 */

	//fmt.Println(maxDepth(tree))
	//fmt.Println(maxDepth(&TreeNode{0, nil, nil}))
	//fmt.Println(isValidBST(bst))
	//fmt.Println(levelOrder(bst))
	//fmt.Println(sortedArrayToBST([]int{1,2,3,4,5}))
	//fmt.Println(firstBadVersion(8))
	//fmt.Println(merge([]int{4,5,6,0,0,0}, 3, []int{1,2,3}, 3))
	fmt.Println(isSymmetric(tree))
}

type TreeNode struct {
	Val int
	Left *TreeNode
	Right *TreeNode
}

func (node *TreeNode) String() string {
	ret := fmt.Sprintf("%d\n", node.Val)
	if node.Left != nil {
		ret += "L:" + node.Left.String() + " "
	}
	if node.Right != nil {
		ret += "R:" + node.Right.String() + " "
	}
	return ret
}

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a int, b int) int {
	if a < b {
		return a
	}
	return b
}


// https://leetcode.com/explore/interview/card/top-interview-questions-easy/94/trees/555/
func maxDepth(root *TreeNode) int {
	return maxDepthHelper(root, 1)
}

func maxDepthHelper(node *TreeNode, curDepth int) int {
	if node == nil {
		return curDepth - 1
	}
	return max(
		maxDepthHelper(node.Left, curDepth + 1),
		maxDepthHelper(node.Right, curDepth + 1),
	)
}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/94/trees/625/

func isValidBST(root *TreeNode) bool {
	const MaxUint = ^uint32(0)
	const MinUint = 0
	const MaxInt = int(MaxUint >> 1) + 1
	const MinInt = -MaxInt - 2

	return bstHelper(root, MinInt, MaxInt)
}

// Takes a node, comparison value, and whether we're looking for larger
func bstHelper(node *TreeNode, minVal int, maxVal int) bool {
	if node == nil {
		return true
	}
	if node.Val <= minVal || node.Val >= maxVal {
		return false
	}
	return bstHelper(node.Left, minVal, node.Val) && bstHelper(node.Right, node.Val, maxVal)
}

func levelOrder(root *TreeNode) [][]int {
	if root == nil {
		return [][]int{}
	}
	var ret [][]int
	levelHelper(root, 0, &ret)
	return ret
}

func levelHelper(node *TreeNode, curLevel int, order *[][]int) {
	if node == nil {
		return
	}
	// Initialize current level
	if len(*order) == curLevel {
		*order = append(*order, []int{})
	}
	// Append current node
	(*order)[curLevel] = append((*order)[curLevel], node.Val)
	// Move on to children
	levelHelper(node.Left, curLevel + 1, order)
	levelHelper(node.Right, curLevel + 1, order)
}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/94/trees/631/
func sortedArrayToBST(nums []int) *TreeNode {
	// Take the midpoint of the array as a pivot, split into 'l' / 'r'
	// Create a node from the pivot, recursively create children of left and r using same strategy
	if len(nums) == 0 {
		return nil
	}
	//pivot := len(nums) / 2
	//node := &TreeNode{nums[pivot], nil, nil}
	//sortedHelper(node, nums[:pivot], nums[pivot+1:])
	return sortedHelper2(nums, 0, len(nums)-1)
}

func sortedHelper(node *TreeNode, left []int, right []int) {
	if len(left) == 0 {
		node.Left = nil
	} else {
		lPivot := len(left) / 2
		node.Left = &TreeNode{left[lPivot], nil, nil}
		sortedHelper(node.Left, left[:lPivot], left[lPivot+1:])
	}

	if len(right) == 0 {
		node.Right = nil
	} else {
		rPivot := len(right) / 2
		node.Right = &TreeNode{right[rPivot], nil, nil}
		sortedHelper(node.Right, right[:rPivot], right[rPivot+1:])
	}
}

func sortedHelper2(nums []int, low int, high int) *TreeNode {
	if low > high {
		return nil
	}
	// Calculate midpoint, use this method to avoid overflow
	mid := low + (high - low) / 2
	node := &TreeNode{nums[mid], nil, nil}
	node.Left = sortedHelper2(nums, low, mid -1)
	node.Right = sortedHelper2(nums, mid + 1, high)
	return node
}

func isBadVersion(version int) bool {
	return version >= 3
}

func firstBadVersion(n int) int {
	return firstBadHelper2(1, n)

}

// I think this is correct, but stack overflows...
func firstBadHelper(low int, high int) int {
	// Git bisect!
	// Take the guess in the middle
	// If bad, take the guess in the middle of the lower half
	// If not bad, take a guess in the middle of the upper half
	// Repeat until done
	if low == high {
		return low
	}
	guess := low + (high - low) / 2
	if isBadVersion(guess) == false {
		return firstBadHelper(guess + 1, high)
	} else {
		return firstBadHelper(low, guess - 1)
	}
}

func firstBadHelper2(low int, high int) int {
	for low < high {
		guess := low + (high - low) / 2
		if isBadVersion(guess) {
			high = guess
		} else {
			low = guess + 1
		}
	}
	return low
}

func merge2(nums1 []int, m int, nums2 []int, n int)  []int {
	// Naive, make a new array
	ret := make([]int, n + m)
	i, j := 0, 0
	for {
		if i < m && nums1[i] < nums2[j] {
			ret[i+j] = nums1[i]
			i++
		} else if j < n {
			ret[i+j] = nums2[j]
			j++
		}
		if i + j == len(ret) {
			break
		}
	}
	return ret
}

// Smarter, fill from the back!
func merge(nums1 []int, m int, nums2 []int, n int)  []int {
	// i and j are our nums1 and nums2 indices
	i, j := m - 1, n - 1
	// k is our current index of the returning array
	k := n + m - 1

	for i >= 0 && j >= 0 {
		if nums1[i] > nums2[j] {
			nums1[k] = nums1[i]
			k--; i--
		} else {
			nums1[k] = nums2[j]
			k--; j--
		}
	}
	for j >= 0 {
		nums1[k] = nums2[j]
		j--; k--
	}

	return nums1
}


func isSymmetric(root *TreeNode) bool {
	vals := make([][]int, 1)
	isSymmetricHelper(root, 0, &vals)
	for _, arr := range vals {
		for j, k := 0, len(arr) - 1; j <= k; j, k = j+1, k-1 {
			if arr[j] != arr[k] {
				return false
			}
		}
	}
	return true
}

func isSymmetricHelper(node *TreeNode, level int, vals *[][]int) {
	if len(*vals) == level {
		*vals = append(*vals, []int{})
	}

	// Nil node, use -101 (out of range) as sentinel nil value
	if node == nil {
		(*vals)[level] = append((*vals)[level], -101)
		return
	}

	(*vals)[level] = append((*vals)[level], node.Val)
	isSymmetricHelper(node.Left, level + 1, vals)
	isSymmetricHelper(node.Right, level + 1, vals)
}