package main

import "fmt"

func main() {
	ms := Constructor()
	ms.Push(1)
	ms.Push(2)
	ms.Push(3)
	fmt.Println(ms.Top())
	fmt.Println(ms.GetMin())
	ms.Pop()
	ms.Pop()
	fmt.Println(ms.Top())
	ms.Pop()

}

/*
	At most 3 * 10^4 calls will be made to push, pop, top, and getMin.

	// We can use a linked list structure here, since access is LIFO, the current min val
	// on a per-node basis will always represent a value of itself, or prior, so never
	// need to search or recompute
 */

type MinStack struct {
	head *Node
}

type Node struct {
	Val int
	Min int
	Next *Node
}


/** initialize your data structure here. */
func Constructor() MinStack {
	return MinStack{}
}


func (this *MinStack) Push(val int)  {
	if this.head == nil {
		this.head = &Node{val, val, nil}
	} else {
		this.head = &Node{val, min(this.head.Min, val), this.head}
	}
}


func (this *MinStack) Pop()  {
	if this.head != nil {
		this.head = this.head.Next
	}
}


func (this *MinStack) Top() int {
	return this.head.Val
}


func (this *MinStack) GetMin() int {
	return this.head.Min
}

func min(a int, b int) int {
	if a < b {
		return a
	}
	return b
}