package main

import "fmt"

func main() {
	//ll := &ListNode{1, &ListNode{2, &ListNode{3, &ListNode{4, nil}}}}
	//ll2 := &ListNode{1, &ListNode{2, &ListNode{3, &ListNode{4, nil}}}}
	//ll2 := &ListNode{1, &ListNode{2, &ListNode{2, &ListNode{1, nil}}}}
	//ll3 := &ListNode{1, &ListNode{2, &ListNode{1, nil}}}
	//fmt.Printf("%v\n", ll)
	//rl := reverseList(ll)
	//rl := reverseListRec(ll)
	//fmt.Println(isPalindrome(ll3))
	//fmt.Println(mergeTwoLists(ll, ll2))
	//fmt.Println(mergeTwoListsRec(ll, ll2))
	removeNthFromEnd(&ListNode{1, nil}, 1)
}

func (node *ListNode) String() string {
	ret := fmt.Sprintf("%d ", node.Val)
	if node.Next != nil {
		ret += node.Next.String()
	}
	return ret
}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/93/linked-list/553/
type ListNode struct {
	Val  int
	Next *ListNode
}

// We get access to the node that needs deleting, not the head
// So we don't delete the node at all, we copy those values in
func deleteNode(node *ListNode) {
	if node == nil {
		return
	}
	if node.Next != nil {
		node.Val = node.Next.Val
		if node.Next.Next == nil {
			node.Next = nil
		}
		deleteNode(node.Next)
	}
}

// Iterative
func reverseList(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}

	var prev *ListNode = nil
	node := head

	for {
		// Save the next node
		temp := node.Next
		// Re-link the old node
		node.Next = prev
		// Record this node as the previous entry
		prev = node
		// If the next node is nil, we're at the new head
		if temp == nil {
			return node
		}
		// Move to the next one
		node = temp
	}
	return node
}

// Recursive
func reverseListRec(head *ListNode) *ListNode {
	// Base case, if we're nil, return nil
	// Base case, next node is null, return the head
	if head == nil || head.Next == nil {
		return head
	}

	// Recursive call to reverse the next link
	node := reverseListRec(head.Next)
	// Adjust the next link back to this node
	head.Next.Next = head
	// Adjust this link to be nil, to be cleaned up in the next call (unless last entry)
	head.Next = nil
	return node
}

func isPalindrome(head *ListNode) bool {
	// If we have 1 element, it's a palindrome
	if head.Next == nil {
		return true
	}
	slow, fast := head, head
	for fast != nil && fast.Next != nil {
		fast = fast.Next.Next
		slow = slow.Next
	}
	// Once fast is nil, slow is at the midpoint
	rev := reverseList(slow)
	for rev != nil {
		if rev.Val != head.Val {
			return false
		}
		rev = rev.Next
		head = head.Next
	}
	return true
}

// Iterative
func mergeTwoLists(l1 *ListNode, l2 *ListNode) *ListNode {
	if l1 == nil {
		return l2
	}
	if l2 == nil {
		return l1
	}

	// We have two linked lists, l1 and l2
	// On each step, pick the current highest and use it as the new next node
	// Repeat

	// Initialize the starting node with a sentinel value
	var head *ListNode = &ListNode{-1000, nil}
	node := head

	for l1 != nil && l2 != nil {
		if l1.Val < l2.Val {
			node.Next = l1
			l1 = l1.Next
		} else {
			node.Next = l2
			l2 = l2.Next
		}
		node = node.Next
	}
	// One of these may be null now, so fix the rest
	if l1 == nil {
		node.Next = l2
	} else {
		node.Next = l1
	}
	// Skip the sentinel value
	return head.Next
}

// Recursive
func mergeTwoListsRec(l1 *ListNode, l2 *ListNode) *ListNode {
	if l1 == nil {
		return l2
	} else if l2 == nil {
		return l1
	}

	if l1.Val < l2.Val {
		l1.Next = mergeTwoListsRec(l1.Next, l2)
		return l1
	} else {
		l2.Next = mergeTwoListsRec(l1, l2.Next)
		return l2
	}
}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/93/linked-list/603/
func removeNthFromEnd(head *ListNode, n int) *ListNode {
	// Lame solution, count nodes, iterate twice
	ctr := 0
	// Count the nodes
	for node := head; node != nil; node, ctr = node.Next, ctr+1 {
	}
	if ctr == 1 && n == 1 {
		return nil
	}
	// Scan through again to delete
	for node, i := head, 0; node != nil; node, i = node.Next, i+1 {
		// If I'm the head and should be deleted, just return the next node
		if i == ctr-n {
			return node.Next
		}
		// If the next node should be deleted, delete it
		if i+1 == ctr-n {
			node.Next = node.Next.Next
			break
		}
	}
	return head
}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/93/linked-list/773/
func hasCycle(head *ListNode) bool {
	slow := head
	fast := head
	for {
		if slow == nil || fast == nil {
			return false
		}
		if fast.Next != nil {
			slow = slow.Next
			fast = fast.Next.Next
		} else {
			return false
		}

		if slow == fast {
			return true
		}
	}
	return false

}
