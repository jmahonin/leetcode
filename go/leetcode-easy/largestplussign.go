// https://leetcode.com/problems/largest-plus-sign/
package main

import (
	"fmt"
)

func main() {
	//mines := [][]int{{4, 2}}
	mines := [][]int{{0,0}}
	fmt.Println(orderOfLargestPlusSign(5, mines))
}

func printGrid(grid [][]int) {
	for i := range grid {
		fmt.Println(grid[i])
	}
}

func orderOfLargestPlusSign(n int, mines [][]int) int {
	// Initialize a grid of ideal values
	grid := make([][]int, n)
	for i := range grid {
		grid[i] = make([]int, n)
		for j := range grid[i] {
			grid[i][j] = min4(i + 1, n - i, j + 1, n - j)
		}
	}
	fmt.Println("Init")
	printGrid(grid)

	// Given mines, adjust values as necessary
	for i := range mines {
		x := mines[i][0]
		y := mines[i][1]

		// For each row/col
		for i := 0; i < n; i++ {
			grid[i][y] = min2(grid[i][y], abs(i - x))
			grid[x][i] = min2(grid[x][i], abs(i - y))
		}
	}

	fmt.Println("Mined")
	printGrid(grid)

	// Find max
	maxVal := 0
	for i := range grid {
		for j := range grid[i] {
			maxVal = max2(maxVal, grid[i][j])
		}
	}
	return maxVal
}

func max2(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func abs(a int) int {
	if a < 0 {
		return a * -1
	}
	return a
}
func min4(a int, b int, c int, d int) int {
	return min2( min2(a, b), min2(c, d) )
}

func min2(a int, b int) int {
	if a < b {
		return a
	}
	return b
}
