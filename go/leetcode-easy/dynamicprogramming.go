package main

import "fmt"

func main () {
	//fmt.Println(climbStairs(44))
	//fmt.Println(maxProfit([]int{7, 1, 5, 3, 6, 4}))
	//fmt.Println(maxSubArray([]int{-2,1,-3,4,-1,2,1,-5,4}))
	fmt.Println(robFib([]int{2,7,9,3,1}))
}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/97/dynamic-programming/569/
func climbStairs(n int) int {
	// Going to need some storage
	dp := make([]int, n+1)

	// The number of ways to reach the i'th step is equal to the sum of the number of ways to reach steps i-1
	// and i - 2

	// This ends up just being Fibonacci
	if n == 0 || n == 1 || n == 2 {
		return n
	}
	dp[1] = 1
	dp[2] = 2
	for i := 3; i <= n; i++ {
		dp[i] = dp[i - 1] + dp[i - 2]
	}
	return dp[n]
}

// https://leetcode.com/explore/interview/card/top-interview-questions-easy/97/dynamic-programming/572/
func maxProfit(prices []int) int {
	// Choose to buy or sell (or do nothing) based on daily price
	// Return the maximum profit you can achieve

	// Need to record some form of state here, is it even buy / sell decisions, or just intervals?


	// profit day[0] -> day[1] = -6, day[N] < 0
	// profit day[1] -> day[2] = 4, day[1] -> day[4] = 5

	// Just record max profit by selling day, and build off that?

	// mp[0] = 0
	// mp[1] = 0
	// mp[2] = 4 (5 - 1)
	// mp[3] = 4
	// mp[4] = 4 (mp[2] + 3 (6 - 3)
	// mp[5] = 7 (mp[2] + mp[4])

	// Once we've recorded new profit, the cycle begins anew


	maxProfit := 0
	//mp := make([]int, len(prices))

	// // [7,1,5,3,6,4]

	for i:= 0; i < len(prices); i++ {
		// Take current day as a buy, run it through the rest of the days to calculate sell profit
		for j:= i + 1; j < len(prices); j++ {
			// Calculate profit
			profit := prices[j] - prices[i]
			// If the profit is higher than max, update it
			if profit > maxProfit {
				maxProfit = profit
			}
			// If the profit on the day is larger
			/*
			if prices[j] - prices[i] > mp[j] {
				mp[j] = prices[j] - prices[i]
				curProfit += mp[j]
			}*/
		}
	}
	return maxProfit
}

func maxSubArray(nums []int) int {
	maxSum := nums[0]
	curSum := nums[0]

	for _, v := range nums {
		// Our current sum can either be the sum + v, or we switch to v
		curSum = max(curSum + v, v)
		// Evaluate if this new sum is the max, or keep the old one
		maxSum = max(maxSum, curSum)
	}
	return maxSum
}

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

// 2,7,9,3,1
func rob(nums []int) int {
	if len(nums) == 0 {
		return 0
	}

	// Make our memoization storage and initialize
	memo := make([]int, len(nums) + 1)
	memo[0] = 0
	memo[1] = nums[0]

	for i := 1; i < len(nums); i ++ {
		// The next option will be either what we're looking at now, or the previous value plus current
		memo[i + 1] = max(memo[i], memo[i-1] + nums[i])
	}

	return memo[len(nums)]
}

// Optimization of the above as a fibonacci
func robFib(nums[] int) int {
	// Initialize
	prev1 := 0
	prev2 := 0

	for _, v := range nums {
		// Update prev1 to be either the new total, or what it was
		tmp := prev1
		prev1 = max(prev1, prev2 + v)
		// Update prev2 to be last prev1
		prev2 = tmp
	}
	return prev1
}