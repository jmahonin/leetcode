package main

import (
	"fmt"
	"math"
	"sort"
)

type Location struct {
	name string
	price int
}

func main() {
	input := []Location{
		{"London", 72}, {"New York", 70}, {"Tokyo", 67}, {"Miami", 62},
	}

	/*
	input := []Location{
		{"London", 72}, {"New York", 72}, {"Tokyo", 72}, {"Miami", 72},
	}
	 */

	mp := map[string]int{
		"London": 72,
		"New York": 70,
		"Tokyo": 67,
		"Miami": 62,
	}

	fmt.Println(max_profitMap(mp))
	fmt.Println(max_profitFast(input))
}

func max_profit(input []Location) []string {
	// Given the input, use Go's sort.Slice to sort this sucker
	sort.Slice(input, func(i, j int) bool {
		return input[i].price > input[j].price
	})

	ret := make([]string, 2)
	ret[0] = input[0].name
	ret[1] = input[len(input)-1].name
	return ret
}

func max_profitMap(input map[string]int) []string {
	// Get the keys into an array
	var keys []string
	for k := range input {
		keys = append(keys, k)
	}

	// Now sort, but use the map value as a comparison
	sort.Slice(keys, func(i, j int) bool {
		return input[keys[i]] < input[keys[j]]
	})

	return []string{keys[0], keys[len(keys)-1]}
}
func max_profitFast(input []Location) []string {
	if len(input) == 0 {
		return nil
	}
	minVal := math.MaxInt32
	maxVal := math.MinInt32
	minLoc, maxLoc := input[0].name, input[0].name

	for i := range input {
		if input[i].price < minVal {
			minVal = input[i].price
			minLoc = input[i].name
		}
		if input[i].price > maxVal {
			maxVal = input[i].price
			maxLoc = input[i].name
		}
	}

	return []string{minLoc, maxLoc}
}
