// From
// https://tutorialedge.net/golang/consuming-restful-api-with-go/
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"sort"
)

type Response struct {
	Name		string			`json:"name"`
	Pokemon		[]Pokemon		`json:"pokemon_entries"`
}

type Pokemon struct {
	EntryNo		int				`json:"entry_number"`
	Species		PokemonSpecies	`json:"pokemon_species"`
}

type PokemonSpecies struct {
	Name 		string			`json:"name"`
}

func main() {
	const endpoint = "http://pokeapi.co/api/v2/pokedex/kanto/"

	response, err := http.Get(endpoint)
	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}

	// Parse the response
	var responseObject Response
	json.Unmarshal(responseData, &responseObject)
	fmt.Println(responseObject.Name)

	// Sort them
	var strs []string
	for _, pokemon := range responseObject.Pokemon {
		strs = append(strs, pokemon.Species.Name)
	}

	sort.Strings(strs)
	fmt.Println(strs)
}
