package main

import (
	"fmt"
	"math"
	"strings"
)

func main() {
	//input := []string{"test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"}
	//input := []string{"a@leetcode.com","b@leetcode.com","c@leetcode.com"}
	//fmt.Println(numUniqueEmails(input))

	//fmt.Println(totalFruit([]int{3,3,3,1,2,1,1,2,3,3,4}))
	//fmt.Println(totalFruit([]int{0,1,6,6,4,4,6}))
	//fmt.Println(totalFruit([]int{3,3,3,1,2,1,1,2,3,3,4}))
	fmt.Println(totalFruit([]int{1,0,3,4,3}))
	//fmt.Println(totalFruit([]int{1,2,1}))
}

// Alice.Z -> AliceZ
// AliceZ+FooBar -> AliceZ
func numUniqueEmails(emails []string) int {
	res := make(map[string]bool, len(emails))

	for i := range emails {
		// alicez@leetcode.com
		// First strip the local name from the domain
		spl := strings.Split(emails[i], "@")
		// TODO: error handling?
		domainName := spl[1]

		// If we have any plusses, take those off first
		// Then if we have any dots, pull those out
		localName := strings.ReplaceAll(
			strings.Split(spl[0], "+")[0],
			".",
			"",
		)

		// So now we have a canonical localName and domain, re-assemble and put into the map
		res[localName + "@" + domainName] = true
	}
	//fmt.Println(res)
	return len(res)
}

// First attempt, I think this is functionally correct but slow
func totalFruit1(tree []int) int {
	curMax := 0
	seen := make(map[int]bool, 3)

	// Iterate through, picking new starting points
	for i := range tree {
		// Run the length, finding if we can get a new max
		j := i
		for j < len(tree) {
			// Get the new tree type
			treeType := tree[j]
			// Mark it as seen
			seen[treeType] = true
			// If we've exceeded our cap, wipe the 'seen' map and break the loop
			if len(seen) > 2 {
				seen = make(map[int]bool, 3)
				break
			}
			// Otherwise, determine the count and see if we have a new max
			count := j - i + 1
			curMax = max(curMax, count)
			// Increment j for the next loop
			j++
		}
	}
	return curMax
}

// Second attempt, didn't quite get there in time
func totalFruit2(tree []int) int {
	seen := make(map[int]int)
	start, curMax := 0, 0

	for i := range tree {
		seen[tree[i]] = i
		if len(seen) > 2 {
			// Find min index (as value) and delete it
			var minVal = math.MaxInt32
			minKey := 0
			for k, v := range seen {
				if v < minVal {
					minVal = v
					minKey = k
				}
			}
			delete(seen, minKey)
			start = minVal + 1
		}
		curMax = max(curMax, i - start + 1)
	}
	return curMax
}

// Third attempt, uses two pointers instead of a map
func totalFruit(tree []int) int {
	first := 0
	second := -1
	curMax := 0
	curCount := 0
	for i := range tree {
		curCount++
		if tree[i] == tree[first] {
			first = i
		} else if second == -1 || tree[i] == tree[second] {
			second = i
		} else {
			// Got a new one, rotate pointers
			if first > second {
				curCount = i - second
				second = i
			} else {
				curCount = i - first
				first = i
			}

		}
		curMax = max(curMax, curCount)
	}
	return curMax
}


func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func min(a int, b int) int {
	if a < b {
		return a
	}
	return b
}