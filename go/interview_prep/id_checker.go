package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	// Use os.args to pull command line args into checkId
	if checkId("Jigarius", "Caesar", "CAJI202002196") {
		fmt.Println("PASS")
	} else {
		fmt.Println("INVESTIGATE")
	}

}

func checkId(firstName string, lastName string, code string) bool {

	// Fail fast
	if len(code) < 12 || len(code) > 18 {
		return false
	}

	var verif int
	var err error

	// First two characters of code must equal first two of lastName (upper)
	if strings.ToUpper(lastName[0:2]) != code[0:2] {
		return false
	}
	// Next two characters must equal first two of firstName
	if strings.ToUpper(firstName[0:2]) != code[2:4] {
		return false
	}

	if verif, err = strconv.Atoi(string(code[len(code)-1])); err != nil {
		return false
	} else {
		// Check the verification digit
		//fmt.Printf("%+d %+d %+d %+d", checkYear, checkMonth, empNo, verif)
		// Get the numeric portion (as a string)
		numStr := code[4:len(code)]
		// Keep our odds and even sums
		var o, e = 0, 0

		// Iterate through the string, not including the verification digit
		for i := 0; i < len(numStr) - 1; i++ {
			// If odd index
			if i % 2 == 0 {
				if v, err := strconv.Atoi(string(numStr[i])); err == nil {
					e += v
				}
			} else {
				if v, err := strconv.Atoi(string(numStr[i])); err == nil {
					o += v
				}
			}
		}

		// Calculate checksum as the absolute difference between odds and evens, modulo 10
		checkSum := abs(o - e) % 10

		if checkSum != verif {
			return false
		}
	}

	return true
}

func abs(x int) int {
	if x < 0 {
		return x * -1
	}
	return x
}
