package robot

import (
	"fmt"
	"strings"
)

const DIM = 10

type Robot struct {
	matrix [][]int
	// Keep track of the current location separately so we don't need to dig every time
	x int
	y int
}

func CreateRobot() *Robot {
	ret := &Robot{}
	ret.matrix = make([][]int, DIM)
	for i := range ret.matrix {
		ret.matrix[i] = make([]int, DIM)
	}
	ret.x = 0
	ret.y = 0
	ret.matrix[0][0] = 1
	return ret
}

func (r *Robot) Print() {
	fmt.Println(" " + strings.Repeat("- ", DIM))
	for i := range r.matrix {
		fmt.Printf("|")
		for j := range r.matrix[i] {
			fmt.Printf("%d ", r.matrix[i][j])
		}
		fmt.Printf("|")
		fmt.Println()
	}
	fmt.Println(" " + strings.Repeat("- ", DIM))
	fmt.Println()
}

func (r *Robot) Place(x, y int) {
	// Wipe old location
	r.matrix[r.y][r.x] = 0
	// Update new location
	r.x = x
	r.y = y
	r.matrix[r.y][r.x] = 1
}

func (r *Robot) Up() error {
	if r.y == 0 {
		return fmt.Errorf("Invalid move")
	}
	r.Place(r.x, r.y-1)
	return nil
}

func (r *Robot) Down() error {
	if r.y == DIM - 1 {
		return fmt.Errorf("Invalid move")
	}
	r.Place(r.x, r.y+1)
	return nil
}

func (r *Robot) Left() error {
	if r.x == 0 {
		return fmt.Errorf("Invalid move")
	}
	r.Place(r.x-1, r.y)
	return nil
}

func (r *Robot) Right() error {
	if r.x == DIM-1 {
		return fmt.Errorf("Invalid move")
	}
	r.Place(r.x+1, r.y)
	return nil
}