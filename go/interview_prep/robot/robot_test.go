package robot

import (
	"fmt"
	"testing"
)

func TestCreate(t *testing.T) {
	rob := CreateRobot()
	if rob == nil {
		t.Errorf("Could not create robot")
	}
}

func TestMove(t *testing.T) {
	rob := CreateRobot()
	rob.Print()
	rob.Down()
	rob.Print()
	rob.Down()
	rob.Print()
	rob.Right()
	rob.Print()
	rob.Right()
	rob.Print()
	rob.Left()
	rob.Print()
	rob.Left()
	rob.Print()
	if err := rob.Left(); err != nil {
		fmt.Println(err)
	} else {
		t.Error("Error condition expected and not hit!")
	}
	if rob.x != 0 && rob.y != 2 {
		t.Error("Invalid final robot placement")
	}
}
