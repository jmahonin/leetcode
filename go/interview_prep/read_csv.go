package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

type Record struct {
	month string
	average float64
	years []int
}
func main() {
	// TODO: Figure out relative paths
	// Get handle to file
	f, err := os.Open("/tmp/hurricanes.csv")
	if err != nil {
		log.Fatalln(err)
	}
	// Create CSV reader
	r := csv.NewReader(f)
	r.LazyQuotes = true

	parsedHeader := false
	for {
		// Get records line by line
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalln(err)
		}

		// Skip the first record
		if !parsedHeader {
			parsedHeader = true
			continue
		}

		// Create a reference to our holding type
		rec := &Record{}

		rec.month = record[0]
		if rec.average, err = strconv.ParseFloat(strings.TrimSpace(record[1]), 64); err != nil {
			log.Println(err)
		}

		rec.years = make([]int, len(record)-2)
		for i := 2; i < len(record); i++ {
			if rec.years[i-2], err = strconv.Atoi(strings.TrimSpace(record[i])); err != nil {
				log.Println(err)
			}
		}

		fmt.Printf("%+v\n", rec)
	}
}
