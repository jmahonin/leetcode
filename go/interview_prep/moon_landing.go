package main

import (
	"fmt"
	"os"
)

type Rocket struct {
	x int
	y int
	speed int
}

func main() {
	done := false
	// Input byte buffer
	var b []byte = make([]byte, 1)

	rocket := &Rocket{0, 0, 0}
	status := "ready for launch"
	for !done {
		// Game loop. Print status, read input, update state
		fmt.Printf("(%d, %d) %s\n", rocket.x, rocket.y, status)
		status = ""

		// Read input
		os.Stdin.Read(b)
		switch b[0] {
		case 119: // w
			if rocket.speed < 5 {
				rocket.speed += 1
			} else {
				status = "maximum speed"
			}
		case 115: // s
			if rocket.speed > 1 {
				rocket.speed -= 1
			} else {
				status = "minimum speed"
			}
		case 97: // a
			rocket.x -= 1
			if rocket.x < -5 {
				status = "wrong trajectory"
			}
		case 100: // d
			rocket.x += 1
			if rocket.x > 5 {
				status = "wrong trajectory"
			}
		}

		// update state
		// speed
		rocket.y += rocket.speed
		// check if we made it
		if rocket.y == 250 && rocket.x == 0 {
			status = "on the moon"
		}
	}
}

