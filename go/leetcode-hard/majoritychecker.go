package main

import (
	"fmt"
	"math/rand"
)

// https://leetcode.com/problems/online-majority-element-in-subarray/
func main() {
	//mj := Constructor([]int{1,1,2,2,1,1})
	//fmt.Println(mj.Query(0,5,4))
	//fmt.Println(mj.Query(0,3, 3))
	//fmt.Println(mj.Query(2,3,2))

	mj := Constructor([]int{2,2,2,2,1,2,2,1,1,1,2,1,2,1,2,2})

	//fmt.Println(mj.Query(4,9,6))
	for i := 0; i < 10; i++ {
		fmt.Println(mj.Query(3, 10, 6))
	}

	//fmt.Println(mj.Query(4, 12, 8))
	//fmt.Println(mj.Query(0, 12, 13))

}

type MajorityChecker struct {
	arr []int
}


func Constructor(arr []int) MajorityChecker {
	return MajorityChecker{arr}
}


func (this *MajorityChecker) Query(left int, right int, threshold int) int {
	// Create the subarray of the left/right boundaries
	subArr := this.arr[left:right+1]

	// The element is going to occur most often in the array, at least > 1/2 of the elements
	// Let's keep track of the elements and counts
	/*
	lt := make(map[int]int, len(subArr))
	for i := range subArr {
		lt[subArr[i]]++
		if lt[subArr[i]] == threshold {
			return subArr[i]
		}
	}
	 */
	// Hint method, let's use probabilities!
	// Let's pull 10 random elements. The on that occurs the most is highly likely to be the majority element

	// The threshold works out to be a percentage, ie threshold / (right - left + 1) == X %
	// So if we have e.g. left=2, right=3, threshold=2, that means 100% of the elements must be the same
	// Similarly, right=5, left=0, threshold=4 means that 80% of the elements must be the same

	// Magic number alert!
	n := 30
	lt := make(map[int]int, n)
	for i := 0; i < n; i++ {
		idx := rand.Intn(len(subArr))
		lt[subArr[idx]]++
	}
	// Find the most frequent elem
	maxElem, maxCount := 0, 0
	for k, v := range lt {
		if v > maxCount {
			maxCount = v
			maxElem = k
		}
	}

	// If the frequency is less than expected, then return -1
	actual := float32(maxCount) / float32(n)
	expected := float32(threshold) / float32(right - left + 1)
	if actual < expected  {
		return -1
	}
	return maxElem
}

