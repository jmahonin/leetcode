package main

import "fmt"

func main() {
	fmt.Println(findMedianSortedArrays([]int{1,2,3}, []int{4,5,6}))
}

// https://leetcode.com/problems/median-of-two-sorted-arrays/
/*
Input: nums1 = [1,3], nums2 = [2]
Output: 2.00000
Explanation: merged array = [1,2,3] and median is 2.
Example 2:

Input: nums1 = [1,2], nums2 = [3,4]
Output: 2.50000
Explanation: merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.
 */
func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	// The arrays are independently sorted
	// One idea would be to combine these arrays together then pick the midpoint
	// But that'd be O(n+m) in time and space

	// We can quickly check the head, tail and midpoint to get an idea of what the ordering looks like
	// We can partition the arrays to smaller problem sets (log n+m) and keep track of offsets
	// Until we're reduced down to one or two elements which we can determine the median of

	// Recursive base case hopefully!
	if len(nums1) == 0 && len(nums2) == 1 {
		return float64(nums2[0])
	} else if len(nums1) == 1 && len(nums2) == 0 {
		return float64(nums1[0])
	} else if len(nums1) == 1 && len(nums2) == 1 {
		return float64(nums1[0] + nums2[0]) / 2.0
	}

	// Goal 'k' to reach
	//goal := (len(nums1) + len(nums2)) / 2
	// Midpoints
	//mid1 := len(nums1) / 2
	//mid2 := len(nums2) / 2

	len1 := len(nums1)
	len2 := len(nums2)


	// All elements in nums1 precede nums2
	if nums1[len1-1] < nums2[0] {
		// If nums1 is smaller than nums2, then it's a simple index lookup
		if len1 < len2 {
			if (len1 + len2) % 2 == 0 {
				// Get two values and find median
				return float64(nums2[(len1+len2)/2 - len1] + nums2[(len1+len2)/2 - len1 + 1])

			} else {
				// One value for median
				return float64(nums2[(len1+len2)/2 - len1 + 1])
			}
		}
	} else if nums2[len(nums2)-1] < nums1[0] {
		// All of nums2 precedes nums1, just call us again and flip order
		return findMedianSortedArrays(nums2, nums1)
	} else {
		// Arrays are interleaved, try break this problem apart more
		mid1 := len(nums1) / 2
		mid2 := len(nums2) / 2

		// All values in nums1 at midpoint are below nums2 midpoint
		if nums1[mid1] < nums2[mid2] {

		} else {

		}
	}
}
