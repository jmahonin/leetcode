package main

import (
	"container/heap"
	"fmt"
	"sort"
)

func main() {
	courses := [][]int {
		{100,200},
		{200,1300},
		{1000,1250},
		{2000,3200},
	}

	fmt.Println(scheduleCourse(courses))
}
// https://leetcode.com/problems/course-schedule-iii/

/*
1 <= courses.length <= 10000
1 <= durationi, lastDayi <= 10000
 */

// Let's define a max heap
type IntHeap []int
func (h IntHeap) Len() int { return len(h) }
func (h IntHeap) Less(i, j int) bool { return h[i] > h[j] }
func (h IntHeap) Swap(i, j int) { h[i], h[j] = h[j], h[i] }

func (h *IntHeap) Push(x interface{}) {
	*h = append(*h, x.(int))
}

func (h *IntHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n - 1]
	*h = old[:n-1]
	return x
}

func scheduleCourse(courses [][]int) int {
	// Interval problem with restrictions
	// Need to maximize total number of courses taken (ie > n with least duration)

	// Is there a general strategy here? IE: start with the least-cost courses, and try slot
	// in the larger ones as possible?


	// We start on day 1, and we have a 'lastDay' constraint, so there' some innate ordering here
	// Can we pick a starting point and generate new options?

	// Sort the input courses by end date, greedy
	sort.Slice(courses, func(i, j int) bool {
		return courses[i][1] < courses[j][1]
	})


	current := 0
	// Create a heap to store current values
	h := &IntHeap{}
	heap.Init(h)
	for i := range courses {
		duration := courses[i][0]
		end := courses[i][1]
		current += duration

		// Push the duration to the heap, keeping the max duration at the head
		heap.Push(h, duration)

		// If we can't accomodate this course, pop one with the greatest duration
		// (It'll either be this one, or one bigger)
		if current > end {
			maxDur := (*h)[0]
			current -= maxDur
			heap.Pop(h)
		}
	}

	return h.Len()
}
