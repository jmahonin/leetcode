package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

const POKEMON_URL = "http://pokeapi.co/api/v2/pokedex/kanto/"

type PokemonResponse struct {
	Entries []PokemonEntry `json:"pokemon_entries""`
}

type PokemonEntry struct {
	EntryNo int `json:"entry_number""`
	Species PokemonSpecies `json:"pokemon_species""`
}

type PokemonSpecies struct {
	Name string `json:"name""`
}

// Unit testable
func unmarshallPokemon(data []byte, pokeResp *PokemonResponse) error {
	if err := json.Unmarshal(data, pokeResp); err != nil {
		return err
	}
	return nil
}

func doIt() ([]string, error) {
	resp, err := http.Get(POKEMON_URL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	pokeResp := &PokemonResponse{}
	unmarshallPokemon(body, pokeResp)

	//fmt.Printf("%+v\n", pokeResp)

	var ret []string
	for i := range pokeResp.Entries {
		ret = append(ret, pokeResp.Entries[i].Species.Name)
	}

	return ret, nil
}
