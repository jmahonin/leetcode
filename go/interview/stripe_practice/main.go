package main

import (
	"fmt"
	"sort"
	"strconv"
)

func main() {
	//testData := []string{"foo", "bar", "baz"}
	//sendIt(testData)

	/*
	fmt.Println(nextServerNumber([]int{1,2,3}))
	fmt.Println(nextServerNumber([]int{2,3,4}))
	fmt.Println(nextServerNumber([]int{1,2,4,5}))
	 */

	track := NewTracker()
	fmt.Println(track.allocate("foobar"))
	fmt.Println(track.allocate("foobar"))
	fmt.Println(track.allocate("foobar"))
	fmt.Printf("%+v\n", track)
}

type Tracker struct {
	// apibox -> apibox[1..n]
	Host map[string]Host
	// apibox[1..n] -> apibox
	Host map[string]Host
}

type Host struct {
	parent string
	// Map of server numbers indicating allocation
	allocated map[int]bool
	// Map of full hostnames back to server parent
	allocated_inv map[string]string
}


func (t Tracker) allocate(name string) string {
	host, ok := t.Hosts[name]
	if !ok {
		t.Hosts[name] = Host{name, []int{}}
	}

	var serverNums []int
	for k := range host.allocated {
		serverNums = append(serverNums, k)
	}

	nextNum := nextServerNumber(serverNums)

	// Add to the tracker map
	host.allocated[nextNum] = true

	hostName := fmt.Sprintf("%s%d", name, nextNum)
	// Add to the inverse lookup too
	host.allocated_inv[hostName] = name

	return hostName
}

func (t Tracker) deallocate(hostName string) error {



	servers := t.hosts[name]
}

func NewTracker() *Tracker {
	return &Tracker{make(map[string][]int)}

}

func nextServerNumber(servers []int) int {
	if servers == nil || len(servers) == 0 {
		return 1
	}
	// Sort 'em
	sort.Ints(servers)
	// Find the next smallest by iterating from the first, incrementing by 1 until we find a gap
	// Unless we're alread

	for i := 0; i < len(servers); i++ {
		// If we're seeing a server we've already allocated, keep going
		if i + 1 == servers[i] {
			continue
		}
		// Otherwise return i
		return i + 1
	}

	// And if we've made it this far, all serves are allocated, return the last server number + 1
	return servers[len(servers)-1] + 1
}

/*
func main2() {
	// Retrieve the pokemon species!
	species, err := doIt()
	if err != nil {
		log.Fatalln(err)
	}
	if err := sendIt(species); err != nil {
		log.Fatalln(err)
	}
}
*/
