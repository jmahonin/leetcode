package main

import "testing"

func TestUnmarshall(t *testing.T) {
	// TODO: Put this in a test resource file
	testPayload := `
		{
		  "id": 2,
		  "pokemon_entries": [
			{
			  "entry_number": 1,
			  "pokemon_species": {
				"name": "bulbasaur",
				"url": "https://pokeapi.co/api/v2/pokemon-species/1/"
			  }
			},
			{
			  "entry_number": 2,
			  "pokemon_species": {
				"name": "ivysaur",
				"url": "https://pokeapi.co/api/v2/pokemon-species/2/"
			  }
			}
		  ]
		}
`
	pokeResp := &PokemonResponse{}
	if err := unmarshallPokemon([]byte(testPayload), pokeResp); err != nil {
		t.Error(err)
	}
	if len(pokeResp.Entries) != 2 {
		t.Error("Unexpected number of entries!")
	}
}

func TestSendIt(t *testing.T) {
	testData := []string{"foo", "bar", "baz"}
	if err := sendIt(testData); err != nil {
		t.Error(err)
	}
}