object LargestRectangleHistogram extends App{
    def largestRectangleAreaN2(heights: Array[Int]): Int = {
      // Take slices of increasing size and calculate
      var maxArea = 0
      var largestSubset: Array[Int] = null

      for(i <- 1 to heights.length) {
        for (subset <- heights.sliding(i)) {
          // Height will be a minimum of subset
          print("Subset (" + subset.mkString(" ") + ") ")
          val height = subset.min
          // Total area is height * width (subset length)
          val area = height * subset.length
          print(" Area: " + area + " ")
          if(area > maxArea) {
            print("NEW MAX!")
            maxArea = area
            largestSubset = subset
          }
          println
        }
      }
      maxArea
    }

  def largestRectangleArea(heights: Array[Int]): Int = {
    for(i <- 0 until heights.length) {
      // 
    }
  }

  largestRectangleArea(Array(2,4))

}
