import scala.collection.mutable

object StraightHand extends App {
  def isNStraightHand(hand: Array[Int], W: Int): Boolean = {

    /*
    // Recurse for the remainder that have been pulled out
    def helper2(bag: List[Int], hand: List[Int], W: Int): Boolean = {
      println(bag, hand)
      // Fail fast
      if (hand.length % W != 0) return false
      // Nothing left in the hand, we must have a sorted list
      if (hand.isEmpty) return true

      // For each possible hand combination
      for (perm <- hand.combinations(W)) {
        // If it's sequential
        if (perm.sorted.sliding(2).forall { y => if (y.length == 1) true else math.abs(y(0) - y(1)) == 1 }) {
          // Create a listbuffer from hand, remove current permutation, recurse
          val lb = new collection.mutable.ListBuffer().addAll(hand)
          if (helper(bag ++ perm, (lb --= perm).toList, W)) return true
        }
      }
      false
    }
    */

    def helper(hand: Array[Int], W: Int): Boolean = {
      if (hand.length == 0) return false
      if (hand.length % W != 0) return false

      // Create a treemap and fill it with card counts
      val tm = new collection.mutable.TreeMap[Int, Int]()
      hand.foreach(v => tm.put(v, tm.getOrElse(v, 0) + 1))

      // Iterate through the tree
      while (tm.nonEmpty) {
        // Take the first entry in our map
        val key = tm.head._1
        // Create an index from it to i + W
        for (i <- key until (key + W)) {
          // If there's no entry return false
          // If the value is greater than 1, decrement by 1, otherwise remove it
          tm.get(i) match {
            case None => return false
            case Some(x) => if (x > 1) tm.put(i, x - 1) else tm.remove(i)
          }
        }
      }
      true
    }

    helper(hand, W)
  }

  //println(isNStraightHand(Array(13,15,23,22,25,4,4,29,15,8,23,12,19,24,17,18,11,22,24,17,17,10,23,21,18,14,18,7,6,3,6,19,11,16,11,12,13,8,26,17,20,13,19,22,21,27,9,20,15,20,27,8,13,25,23,22,15,9,14,20,10,6,5,14,12,7,16,21,18,21,24,23,10,21,16,18,16,18,5,20,19,20,10,14,26,2,9,19,12,28,17,5,7,25,22,16,17,21,11), 10))
  println(isNStraightHand(Array(1, 9, 8, 2, 6, 5, 4, 3, 7, 10), 4))
}
