object EnumerateSubsets extends App {
  def enumerate(soFar: String, rest: String): Unit= {
    if(rest == "") {
      println(soFar)
    } else {
      enumerate(soFar + rest.head, rest.substring(1))
      enumerate(soFar, rest.substring(1))
    }
  }

  enumerate("", "abcd")
}
