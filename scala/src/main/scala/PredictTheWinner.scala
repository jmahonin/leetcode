object PredictTheWinner extends App {
  def predict(nums: Array[Int]): Boolean = {
    var turn = 1
    var p1Score, p2Score = 0

    var input = nums.toList

    while(input.nonEmpty) {
      val (max: Int, remain) = input match {
        case Nil => (0, Nil)
        case x :: Nil => (x, Nil)
        case x :: xs => if (x > xs.reverse.head) (x, xs) else (xs.reverse.head, x :: xs.dropRight(1))
      }
      if(turn == 1) {
        p1Score = p1Score + max
        turn = 2;
      } else {
        p2Score = p2Score + max
        turn = 1
      }
      input = remain
    }
    p1Score >= p2Score
  }

  val rand = util.Random
  var arr = (0 until 5).toList.map { x => rand.nextInt(10) }.toArray
  println("p1? " + predict(arr))

}
