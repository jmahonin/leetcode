object RemoveNthNode extends App {
  class ListNode(_x: Int = 0, _next: ListNode = null) {
    var next: ListNode = _next
    var x: Int = _x
  }

  def printNode(head: ListNode): Unit = {
    if(head != null) {
      print(" " + head.x)
      printNode(head.next)
    }
  }

    def removeNthFromBeginning(head: ListNode, n: Int): ListNode = {
      def helper(thisNode: ListNode, n: Int, curr: Int): Unit = {
        // If I or the next node is null, bail
        if(thisNode == null || thisNode.next == null) {
          return
        }
        // If the next node is 'n', prune it
        if(curr + 1 == n) {
          if(thisNode.next != null) {
            // Redirect this node to point to the node after next
            thisNode.next = thisNode.next.next
          }
        }
        helper(thisNode.next, n, curr + 1)
      }
      // Special case for first node
      if(n == 1) {
        head.next
      } else {
        helper(head, n, 1)
        head
      }
    }: ListNode

  def removeNthFromEnd(head: ListNode, n: Int) = {
    // Iterate through and unwind with some state...
    // Probably need to return something useful
    def helper(headNode: ListNode, currNode: ListNode, n: Int, curr: Int): (ListNode, Int) = {
      // If the next node is null, we're at the end of the list.
      // Return the head of the list and the current depth
      if(currNode.next == null) {
        return (headNode, curr)
      } else {
        // Recur to find the max depth
        val (hNode, maxDepth) = helper(headNode, currNode.next, n, curr + 1)
        // As we unwind, fix the linking
        println(maxDepth, n, curr, currNode.x, currNode.next)
        // We can't prune the node when we're on it, we need to go back one level
        if(maxDepth - n == curr + 1) {
          // Fix the link
          if(currNode.next != null) {
            currNode.next = currNode.next.next
          }
        }
        // If we're pruning the head node and we're on it, lop the head off the list
        if(n + 1 == maxDepth && curr == 1) {
          (currNode.next, maxDepth)
        }
        // Otherwise, just return the head and the depth
        else {
          (hNode, maxDepth)
        }
      }
    }
    if(head.next == null && n == 1) {
      // Special handling for 1 node being removed, just return null
      null
    } else {
      helper(head, head, n-1, 1)._1
    }
  }

  //val input = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))))
  val input = new ListNode(1, new ListNode(2, null))
  printNode(input)
  println
  printNode(removeNthFromEnd(input, 1))
  println

  val input2 = new ListNode(1)
  printNode(removeNthFromEnd(input2, 1))
}
