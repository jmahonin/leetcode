object Solution extends App {
  solveNQueens(5)

  def isSafe(queen: (Int, Int), queens: List[(Int, Int)]): Boolean = {
    // Search for conflicts
    queens.forall { q =>
      q._1 != queen._1 && q._2 != queen._2 && (math.abs(q._1 - queen._1) != (math.abs(q._2 - queen._2)))
    }
  }

  def solveNQueens(n: Int): Unit = {
    def placeQueens(k: Int): List[List[(Int, Int)]] = {
      // Base case, empty list of lists
      if (k == 0) {
        List(List())
      } else {
        // Generator pattern. Recursively place queens, iterate over columns, create new
        // queen and add to queens list if safe
        for {
          queens <- placeQueens(k - 1)
          col <- 1 to n
          queen = (k, col)
          if isSafe(queen, queens)
        } yield queen :: queens
      }
    }
    val result = placeQueens(n)
    result.foreach(printBoard(n, _))
  }
  def printBoard(n: Int, queens: List[(Int, Int)]) = {
    val board = Array.ofDim[String](n, n)
    // Fill with "."
    for {
      i <- 0 until n
      j <- 0 until n
    } board(i)(j) = "."

    // Add queens
    queens.foreach{case (row, col) => board(row - 1)(col - 1) = "Q"}
    // Print
    board.foreach{ row => println(row.mkString(" ")) }
    println()

  }
}
