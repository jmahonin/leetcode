import scala.::

object RecPermute extends App {
  def recPermute(soFar: String, rest: String): Unit = {
    if (rest == "") {
      println(soFar)
    } else {
      for(i <- 0 until rest.length) {
        recPermute(
          soFar + rest(i),
          rest.substring(0, i) + rest.substring(i+1)
        )
      }
    }
  }


  def recPermute2(curr: String): List[String] = {
    if(curr.isEmpty) List(curr)
    else {
      for {
        i <- 0 until curr.length
        perm <- recPermute2(curr.slice(0, i) ++ curr.slice(i + 1, curr.length))
      } yield curr(i).toString ++ perm
    }.toList
  }
  println(recPermute2("abc"))
}


